# frozen_string_literal: true

Rails.application.routes.draw do
  # ----------
  # api
  # ----------
  mount_devise_token_auth_for 'User', at: 'api/v1/auth', controllers: {
    registrations: 'api/v1/auth/registrations'
  }
  namespace :api do
    namespace :v1 do
      resources :addresses
      resources :contacts,     only: %i[create]
      resources :coordinates,  only: %i[index show]
      resources :informations, only: %i[index show]
      resources :orders,       only: %i[index show]
      resources :pages,        only: %i[index show]
      resources :products,     only: %i[index show]
      resources :rankings,     only: %i[index show]
      resources :stores,       only: %i[index show]
      resources :store_blogs,  only: %i[index show]
      resources :variations,   only: %i[index show]
    end
  end

  # ----------
  # admin
  # ----------
  devise_for :admins, path: 'admin/auth', skip: %i[registrations], controllers: {
    sessions: 'admin/auth/sessions',
    unlocks:  'admin/auth/unlocks'
  }
  # only use update(disable sign up)
  as :admin do
    get 'admin/auth/edit' => 'admin/auth/registrations#edit', as: 'edit_admin_registration'
    put 'admin/auth' => 'admin/auth/registrations#update', as: 'admin_registration'
  end
  namespace :admin do
    root 'dashboards#show'
    resources :admins
    resources :brands
    resources :categories
    resources :colors
    resources :companies
    resources :contacts
    resources :coordinates
    resources :divisions
    resources :informations
    resources :pages
    resources :positions
    resources :products
    resources :orders
    resources :rankings
    resources :sizes
    resources :staffs
    resources :stores
    resources :store_blogs
    resources :tags
    resources :users
    resources :variations
  end
end
