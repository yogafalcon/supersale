RSpec.describe ProductDecorator do
  describe '#category_list' do
    subject { decorate(product).category_list }
    context 'provide existent category' do
      let(:product) { create(:product, :many_categories) }
      it { is_expected.to eq '<ul class="list-group"><li class="list-group-item">name_1</li><li class="list-group-item">name_2</li><li class="list-group-item">name_3</li></ul>' }
    end
    context 'provide non-existent category' do
      let(:product) { create(:product) }
      it { is_expected.to eq '<ul class="list-group"></ul>' }
    end
  end

  describe '#tag_list' do
    subject { decorate(product).tag_list }
    context 'provide existent tag' do
      let(:product) { create(:product, :many_tags) }
      it { is_expected.to eq '<ul class="list-group"><li class="list-group-item">name_1</li><li class="list-group-item">name_2</li><li class="list-group-item">name_3</li></ul>' }
    end
    context 'provide non-existent tag' do
      let(:product) { create(:product) }
      it { is_expected.to eq '<ul class="list-group"></ul>' }
    end
  end
end
