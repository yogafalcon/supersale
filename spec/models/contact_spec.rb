# == Schema Information
#
# Table name: contacts
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  email      :string(255)
#  category   :integer
#  content    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Contact, type: :model do
  describe 'enum' do
    it { is_expected.to define_enum_for(:category).with(%i[about_product about_order]) }
  end

  describe 'validates' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:category) }
    it { is_expected.to validate_presence_of(:content) }
  end
end
