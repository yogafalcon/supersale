# == Schema Information
#
# Table name: colors
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  code       :string(255)
#  hex_code   :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Color, type: :model do
  describe 'association' do
    it { is_expected.to have_many(:variations).dependent(:restrict_with_error) }
  end
end
