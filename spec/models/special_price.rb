# == Schema Information
#
# Table name: special_prices
#
#  id         :bigint(8)        not null, primary key
#  product_id :bigint(8)
#  price      :integer
#  begin_datetime   :datetime
#  end_datetime     :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe SpecialPrice, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:product) }
  end
end
