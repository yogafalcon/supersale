# == Schema Information
#
# Table name: coordinates
#
#  id         :bigint(8)        not null, primary key
#  staff_id   :bigint(8)
#  point_1    :text(65535)
#  point_2    :text(65535)
#  point_3    :text(65535)
#  comment    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Coordinate, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:staff) }
    it { is_expected.to have_many(:coordinates_products).dependent(:destroy) }
    it { is_expected.to have_many(:products).through(:coordinates_products) }
  end
end
