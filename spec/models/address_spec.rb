# == Schema Information
#
# Table name: addresses
#
#  id            :bigint(8)        not null, primary key
#  user_id       :bigint(8)
#  first_name    :string(255)
#  last_name     :string(255)
#  first_kana    :string(255)
#  last_kana     :string(255)
#  postal_code   :string(255)
#  prefecture_id :bigint(8)
#  address_1     :string(255)
#  address_2     :string(255)
#  address_3     :string(255)
#  tel_no        :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Address, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:prefecture) }
  end
end
