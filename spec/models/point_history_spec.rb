# == Schema Information
#
# Table name: point_histories
#
#  id             :bigint(8)        not null, primary key
#  user_id        :bigint(8)
#  category       :integer
#  point          :integer
#  content        :text(65535)
#  begin_datetime :datetime
#  end_datetime   :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe PointHistory, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:user) }
  end

  describe 'enum' do
    it { is_expected.to define_enum_for(:category).with(%i[normal limited_time expired]) }
  end
end
