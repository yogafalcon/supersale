# == Schema Information
#
# Table name: orders
#
#  id             :bigint(8)        not null, primary key
#  category       :integer
#  status         :integer
#  payment_status :integer
#  user_id        :bigint(8)
#  first_name     :string(255)
#  last_name      :string(255)
#  first_kana     :string(255)
#  last_kana      :string(255)
#  postal_code    :string(255)
#  prefecture_id  :bigint(8)
#  address_1      :string(255)
#  address_2      :string(255)
#  address_3      :string(255)
#  tel_no         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe Order, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:prefecture) }
    it { is_expected.to have_many(:orders_variations).dependent(:destroy) }
    it { is_expected.to have_many(:variations).through(:orders_variations) }
  end

  describe 'enum' do
    it { is_expected.to define_enum_for(:category).with(%i[normal pre_order back_order]) }
    it { is_expected.to define_enum_for(:status).with(%i[waiting_allocation waiting_shipping shipped cancel]) }
    it { is_expected.to define_enum_for(:payment_status).with(%i[waiting_payment paid waiting_refundment refunded]) }
  end
end
