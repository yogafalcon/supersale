# == Schema Information
#
# Table name: rankings
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Ranking, type: :model do
  describe 'association' do
    it { is_expected.to have_many(:products_rankings).dependent(:destroy) }
    it { is_expected.to have_many(:products).through(:products_rankings) }
  end
end
