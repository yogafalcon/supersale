# == Schema Information
#
# Table name: categories_products
#
#  id          :bigint(8)        not null, primary key
#  category_id :bigint(8)
#  product_id  :bigint(8)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe CategoriesProduct, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:category) }
    it { is_expected.to belong_to(:product) }
  end
end
