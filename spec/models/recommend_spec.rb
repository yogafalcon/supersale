# == Schema Information
#
# Table name: recommends
#
#  id                   :bigint(8)        not null, primary key
#  category             :integer
#  product_id           :bigint(8)
#  recommend_product_id :bigint(8)
#  priority             :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'rails_helper'

RSpec.describe Recommend, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:product) }
    it { is_expected.to belong_to(:recommend_product) }
  end

  describe 'enum' do
    it { is_expected.to define_enum_for(:category).with(%i[also_bought]) }
  end
end
