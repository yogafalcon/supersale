# == Schema Information
#
# Table name: products_tags
#
#  id         :bigint(8)        not null, primary key
#  product_id :bigint(8)
#  tag_id     :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe ProductsTag, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:product) }
    it { is_expected.to belong_to(:tag) }
  end
end
