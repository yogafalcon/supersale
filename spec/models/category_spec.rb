# == Schema Information
#
# Table name: categories
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'association' do
    it { is_expected.to have_many(:categories_products).dependent(:destroy) }
    it { is_expected.to have_many(:products).through(:categories_products) }
  end
end
