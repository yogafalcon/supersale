# == Schema Information
#
# Table name: coordinates_products
#
#  id            :bigint(8)        not null, primary key
#  coordinate_id :bigint(8)
#  product_id    :bigint(8)
#  priority      :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe CoordinatesProduct, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:coordinate) }
    it { is_expected.to belong_to(:product) }
  end
end
