# == Schema Information
#
# Table name: staffs
#
#  id          :bigint(8)        not null, primary key
#  division_id :bigint(8)
#  position_id :bigint(8)
#  store_id    :bigint(8)
#  name        :string(255)
#  code        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Staff, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:division) }
    it { is_expected.to belong_to(:position) }
    it { is_expected.to belong_to(:store) }
  end
end
