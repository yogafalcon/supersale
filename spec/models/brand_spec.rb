# == Schema Information
#
# Table name: brands
#
#  id         :bigint(8)        not null, primary key
#  company_id :bigint(8)
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Brand, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:company) }
    it { is_expected.to have_many(:products).dependent(:restrict_with_error) }
  end
end
