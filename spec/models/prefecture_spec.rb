# == Schema Information
#
# Table name: prefectures
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Prefecture, type: :model do
  describe 'association' do
    it { is_expected.to have_many(:addresses).dependent(:restrict_with_error) }
    it { is_expected.to have_many(:stores).dependent(:restrict_with_error) }
  end
end
