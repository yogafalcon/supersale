# == Schema Information
#
# Table name: orders_variations
#
#  id           :bigint(8)        not null, primary key
#  order_id     :bigint(8)
#  variation_id :bigint(8)
#  quantity     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe OrdersVariation, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:order) }
    it { is_expected.to belong_to(:variation) }
  end
end
