# == Schema Information
#
# Table name: pages
#
#  id            :bigint(8)        not null, primary key
#  category      :integer
#  reference_url :string(255)
#  subject       :string(255)
#  content       :text(65535)
#  priority      :integer
#  enabled       :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Page, type: :model do
  describe 'enum' do
    it { is_expected.to define_enum_for(:category).with(%i[normal feature main_feature sub_feature key_visual]) }
  end
end
