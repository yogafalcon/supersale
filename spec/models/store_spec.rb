# == Schema Information
#
# Table name: stores
#
#  id            :bigint(8)        not null, primary key
#  name          :string(255)
#  code          :string(255)
#  postal_code   :string(255)
#  prefecture_id :bigint(8)
#  address_1     :string(255)
#  address_2     :string(255)
#  address_3     :string(255)
#  tel_no        :string(255)
#  blog_url      :string(255)
#  rss_url       :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Store, type: :model do
  describe 'association' do
    it { is_expected.to have_many(:staffs).dependent(:restrict_with_error) }
    it { is_expected.to belong_to(:prefecture) }
    it { is_expected.to have_one(:store_blog).dependent(:restrict_with_error) }
  end

  describe '#address' do
    subject { store.address }
    let(:store) do
      pref = Prefecture.create(name: 'aichi')
      create(:store, prefecture: pref, address_1: 'ad1', address_2: 'ad2', address_3: 'ad3')
    end
    it { is_expected.to eq 'aichiad1ad2 ad3' }
  end
end
