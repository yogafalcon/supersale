# == Schema Information
#
# Table name: admins
#
#  id                 :bigint(8)        not null, primary key
#  email              :string(255)      default(""), not null
#  encrypted_password :string(255)      default(""), not null
#  sign_in_count      :integer          default(0), not null
#  current_sign_in_at :datetime
#  last_sign_in_at    :datetime
#  current_sign_in_ip :string(255)
#  last_sign_in_ip    :string(255)
#  failed_attempts    :integer          default(0), not null
#  unlock_token       :string(255)
#  locked_at          :datetime
#  role               :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'rails_helper'

RSpec.describe Admin, type: :model do
  describe 'enum' do
    it { is_expected.to define_enum_for(:role).with(%i[staff master owner]) }
  end

  describe 'after_initialize' do
    subject { admin.role }
    context 'set role' do
      let(:admin) { Admin.create(email: 'foo', password: 'bar', role: :owner) }
      it { is_expected.to eq 'owner'}
    end
    context 'no set role' do
      let(:admin) { Admin.create(email: 'foo', password: 'bar') }
      it { is_expected.to eq 'staff'}
    end
  end
end
