# == Schema Information
#
# Table name: products
#
#  id                :bigint(8)        not null, primary key
#  brand_id          :bigint(8)
#  name              :string(255)
#  code              :string(255)
#  caption           :string(255)
#  description       :text(65535)
#  price             :integer
#  point             :integer
#  cost              :integer
#  priority          :integer
#  begin_datetime    :datetime
#  end_datetime      :datetime
#  material          :string(255)
#  country_of_origin :string(255)
#  favorites_count   :integer
#  enabled           :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'rails_helper'

RSpec.describe Product, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:brand) }
    it { is_expected.to have_many(:special_prices).dependent(:destroy) }
    it { is_expected.to have_many(:variations).dependent(:restrict_with_error) }
    it { is_expected.to have_many(:categories_products).dependent(:destroy) }
    it { is_expected.to have_many(:categories).through(:categories_products) }
    it { is_expected.to have_many(:products_tags).dependent(:destroy) }
    it { is_expected.to have_many(:tags).through(:products_tags) }
    it { is_expected.to have_many(:products_rankings).dependent(:destroy) }
    it { is_expected.to have_many(:rankings).through(:products_rankings) }
    it { is_expected.to have_many(:reviews).dependent(:destroy) }
    it { is_expected.to have_many(:coordinates_products).dependent(:destroy) }
    it { is_expected.to have_many(:coordinates).through(:coordinates_products) }
    it { is_expected.to have_many(:recommends).dependent(:destroy) }
    it { is_expected.to have_many(:favorites).dependent(:destroy) }
  end

  describe '.category' do
    subject { Product.category(category).size }
    before { create(:product, :many_categories) }
    context 'provide existent category' do
      let(:category) { 'code_1' }
      it { is_expected.to eq 1 }
    end
    context 'provide non-existent category' do
      let(:category) { 'code_9' }
      it { is_expected.to be_zero }
    end
  end

  describe '.ransackable_scopes' do
    context 'category case' do
      subject { Product.search(q).result.size }
      before { create(:product, :many_categories) }
      context 'provide existent category' do
        let(:q) { { category: :code_1 } }
        it { is_expected.to eq 1 }
      end
      context 'provide non-existent category' do
        let(:q) { { category: :code_9 } }
        it { is_expected.to be_zero }
      end
    end
    context 'tag case' do
      subject { Product.search(q).result.size }
      before { create(:product, :many_tags) }
      context 'provide existent tag' do
        let(:q) { { tag: :code_1 } }
        it { is_expected.to eq 1 }
      end
      context 'provide non-existent tag' do
        let(:q) { { tag: :code_9 } }
        it { is_expected.to be_zero }
      end
    end
    context 'ranking case' do
      subject { Product.search(q).result.size }
      before { create(:product, :many_rankings) }
      context 'provide existent ranking' do
        let(:q) { { ranking: :code_1 } }
        it { is_expected.to eq 1 }
      end
      context 'provide non-existent ranking' do
        let(:q) { { ranking: :code_9 } }
        it { is_expected.to be_zero }
      end
    end
  end

  describe '#colors' do
    subject { product.colors.size }
    context 'variation is nothing' do
      let(:product) { create(:product) }
      it { is_expected.to be_zero }
    end
    context 'has many variations' do
      let(:product) { create(:product, :many_variations) }
      it { is_expected.to eq 3 }
    end
  end
end
