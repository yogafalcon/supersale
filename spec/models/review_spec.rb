# == Schema Information
#
# Table name: reviews
#
#  id         :bigint(8)        not null, primary key
#  product_id :bigint(8)
#  user_id    :bigint(8)
#  rating     :integer
#  comment    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Review, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:product) }
    it { is_expected.to belong_to(:user) }
  end
end
