# == Schema Information
#
# Table name: variations
#
#  id         :bigint(8)        not null, primary key
#  product_id :bigint(8)
#  color_id   :bigint(8)
#  size_id    :bigint(8)
#  stock      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Variation, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:product) }
    it { is_expected.to belong_to(:color) }
    it { is_expected.to belong_to(:size) }
    it { is_expected.to have_many(:orders_variations).dependent(:restrict_with_error) }
    it { is_expected.to have_many(:orders).through(:orders_variations) }
  end
end
