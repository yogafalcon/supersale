# == Schema Information
#
# Table name: store_blogs
#
#  id         :bigint(8)        not null, primary key
#  store_id   :bigint(8)
#  name       :string(255)
#  subject    :string(255)
#  content    :text(65535)
#  priority   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :store_blog do
    store nil
    subject "MyString"
    content "MyText"
    priority 1
  end
end
