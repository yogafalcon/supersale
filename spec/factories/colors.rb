# == Schema Information
#
# Table name: colors
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  code       :string(255)
#  hex_code   :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :color do
    sequence(:name) { |n| "name_#{n}" }
    sequence(:code) { |n| "code_#{n}" }
    sequence(:hex_code) { |n| "hex_code_#{n}" }
  end
end
