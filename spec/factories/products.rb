# == Schema Information
#
# Table name: products
#
#  id                :bigint(8)        not null, primary key
#  brand_id          :bigint(8)
#  name              :string(255)
#  code              :string(255)
#  caption           :string(255)
#  description       :text(65535)
#  price             :integer
#  point             :integer
#  cost              :integer
#  priority          :integer
#  begin_datetime    :datetime
#  end_datetime      :datetime
#  material          :string(255)
#  country_of_origin :string(255)
#  favorites_count   :integer
#  enabled           :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

FactoryBot.define do
  factory :product do
    sequence(:name) { |n| "name_#{n}" }
    sequence(:code) { |n| "code_#{n}" }
    brand

    trait :many_variations do
      after(:build) do |product|
        colors = create_list(:color, 3)
        sizes = create_list(:size, 3)
        colors.each do |color|
          sizes.each do |size|
            product.variations << build(:variation, product: product, color: color, size: size)
          end
        end
      end
    end

    trait :many_categories do
      after(:build) do |product|
        3.times do |index|
          product.categories << create(:category, code: "code_#{index}")
        end
      end
    end

    trait :many_tags do
      after(:build) do |product|
        3.times do |index|
          product.tags << create(:tag, code: "code_#{index}")
        end
      end
    end

    trait :many_rankings do
      after(:build) do |product|
        3.times do |index|
          product.rankings << create(:ranking, code: "code_#{index}")
        end
      end
    end
  end
end
