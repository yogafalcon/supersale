# == Schema Information
#
# Table name: categories
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :category do
    sequence(:name) { |n| "name_#{n}" }
    sequence(:code) { |n| "code_#{n}" }
  end
end
