# == Schema Information
#
# Table name: pages
#
#  id            :bigint(8)        not null, primary key
#  category      :integer
#  reference_url :string(255)
#  subject       :string(255)
#  content       :text(65535)
#  priority      :integer
#  enabled       :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :page do
    
  end
end
