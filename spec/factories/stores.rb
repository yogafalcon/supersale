# == Schema Information
#
# Table name: stores
#
#  id            :bigint(8)        not null, primary key
#  name          :string(255)
#  code          :string(255)
#  postal_code   :string(255)
#  prefecture_id :bigint(8)
#  address_1     :string(255)
#  address_2     :string(255)
#  address_3     :string(255)
#  tel_no        :string(255)
#  blog_url      :string(255)
#  rss_url       :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :store do
    
  end
end
