# == Schema Information
#
# Table name: brands
#
#  id         :bigint(8)        not null, primary key
#  company_id :bigint(8)
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :brand do
    sequence(:name) { |n| "name_#{n}" }
    sequence(:code) { |n| "code_#{n}" }
    company
  end
end
