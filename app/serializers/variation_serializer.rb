# frozen_string_literal: true

# == Schema Information
#
# Table name: variations
#
#  id         :bigint(8)        not null, primary key
#  product_id :bigint(8)
#  color_id   :bigint(8)
#  size_id    :bigint(8)
#  stock      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class VariationSerializer < ActiveModel::Serializer
  attributes :id, :stock, :image_path
  belongs_to :product
  belongs_to :color
  belongs_to :size
end
