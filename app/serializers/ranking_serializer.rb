# frozen_string_literal: true

# == Schema Information
#
# Table name: rankings
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class RankingSerializer < ActiveModel::Serializer
  attributes :id, :name, :code
end
