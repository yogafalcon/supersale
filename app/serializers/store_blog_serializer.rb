# frozen_string_literal: true

# == Schema Information
#
# Table name: store_blogs
#
#  id         :bigint(8)        not null, primary key
#  store_id   :bigint(8)
#  name       :string(255)
#  subject    :string(255)
#  content    :text(65535)
#  priority   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class StoreBlogSerializer < ActiveModel::Serializer
  attributes :id, :name, :subject, :content, :priority, :updated_at, :image_path
  belongs_to :store
end
