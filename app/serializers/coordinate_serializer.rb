# frozen_string_literal: true

# == Schema Information
#
# Table name: coordinates
#
#  id         :bigint(8)        not null, primary key
#  staff_id   :bigint(8)
#  point_1    :text(65535)
#  point_2    :text(65535)
#  point_3    :text(65535)
#  comment    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CoordinateSerializer < ActiveModel::Serializer
  attributes :id, :point_1, :point_2, :point_3, :comment, :image_paths, :updated_at
  has_one :staff
end
