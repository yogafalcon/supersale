# frozen_string_literal: true

# == Schema Information
#
# Table name: informations
#
#  id               :bigint(8)        not null, primary key
#  release_datetime :datetime
#  subject          :string(255)
#  content          :text(65535)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class InformationSerializer < ActiveModel::Serializer
  attributes :id, :release_datetime, :subject, :content
end
