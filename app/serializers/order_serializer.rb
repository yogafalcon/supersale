# frozen_string_literal: true

# == Schema Information
#
# Table name: orders
#
#  id             :bigint(8)        not null, primary key
#  category       :integer
#  status         :integer
#  payment_status :integer
#  user_id        :bigint(8)
#  first_name     :string(255)
#  last_name      :string(255)
#  first_kana     :string(255)
#  last_kana      :string(255)
#  postal_code    :string(255)
#  prefecture_id  :bigint(8)
#  address_1      :string(255)
#  address_2      :string(255)
#  address_3      :string(255)
#  tel_no         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class OrderSerializer < ActiveModel::Serializer
  attributes :id, :category, :status, :payment_status, :last_name, :first_name,
             :last_kana, :first_kana, :postal_code, :address_1, :address_2, :address_3, :tel_no
  has_one :prefecture
  has_many :orders_variations
end
