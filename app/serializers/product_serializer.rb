# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id                :bigint(8)        not null, primary key
#  brand_id          :bigint(8)
#  name              :string(255)
#  code              :string(255)
#  caption           :string(255)
#  description       :text(65535)
#  price             :integer
#  point             :integer
#  cost              :integer
#  priority          :integer
#  begin_datetime    :datetime
#  end_datetime      :datetime
#  material          :string(255)
#  country_of_origin :string(255)
#  favorites_count   :integer
#  enabled           :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class ProductSerializer < ActiveModel::Serializer
  attributes :id, :brand_id, :name, :code, :caption, :description, :price, :point, :cost, :priority,
             :begin_datetime, :end_datetime, :material, :country_of_origin,
             :favorites_count, :enabled,
             :image_paths
  has_many :tags
  has_many :colors
  # has_many :variations
end
