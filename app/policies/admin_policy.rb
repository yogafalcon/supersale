# frozen_string_literal: true

class AdminPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    scope.where(id: record.id).exists?
  end

  def create?
    user.owner?
  end

  def new?
    create?
  end

  def update?
    user.owner?
  end

  def edit?
    update?
  end

  def destroy?
    user.owner? && record.id != user.id
  end
end
