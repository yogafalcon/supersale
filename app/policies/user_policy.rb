# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def index?
    user.master? || user.owner?
  end

  def show?
    super && (user.master? || user.owner?)
  end

  def create?
    false
  end

  def update?
    false
  end

  def destroy?
    false
  end
end
