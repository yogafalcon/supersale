# frozen_string_literal: true

# == Schema Information
#
# Table name: admins
#
#  id                 :bigint(8)        not null, primary key
#  email              :string(255)      default(""), not null
#  encrypted_password :string(255)      default(""), not null
#  sign_in_count      :integer          default(0), not null
#  current_sign_in_at :datetime
#  last_sign_in_at    :datetime
#  current_sign_in_ip :string(255)
#  last_sign_in_ip    :string(255)
#  failed_attempts    :integer          default(0), not null
#  unlock_token       :string(255)
#  locked_at          :datetime
#  role               :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Admin < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :trackable, :validatable, :lockable, :timeoutable

  has_one_attached :avatar

  enum role: %i[staff master owner]

  after_initialize -> { self.role ||= :staff }, if: :new_record?
end
