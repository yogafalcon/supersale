# frozen_string_literal: true

# == Schema Information
#
# Table name: coordinates
#
#  id         :bigint(8)        not null, primary key
#  staff_id   :bigint(8)
#  point_1    :text(65535)
#  point_2    :text(65535)
#  point_3    :text(65535)
#  comment    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Coordinate < ApplicationRecord
  belongs_to :staff, optional: true
  has_many :coordinates_products, dependent: :destroy
  has_many :products, through: :coordinates_products

  has_many_attached :images

  def image_paths
    images.map do |image|
      URI.join(
        Rails.application.config.action_controller.asset_host,
        Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
      )
    end
  end
end
