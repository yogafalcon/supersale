# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id                :bigint(8)        not null, primary key
#  brand_id          :bigint(8)
#  name              :string(255)
#  code              :string(255)
#  caption           :string(255)
#  description       :text(65535)
#  price             :integer
#  point             :integer
#  cost              :integer
#  priority          :integer
#  begin_datetime    :datetime
#  end_datetime      :datetime
#  material          :string(255)
#  country_of_origin :string(255)
#  favorites_count   :integer
#  enabled           :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Product < ApplicationRecord
  prepend Concerns::ImagesPurger

  belongs_to :brand
  has_many :special_prices, dependent: :destroy
  accepts_nested_attributes_for :special_prices, allow_destroy: true
  has_many :variations, dependent: :restrict_with_error
  has_many :categories_products, dependent: :destroy
  accepts_nested_attributes_for :categories_products, allow_destroy: true
  has_many :categories, through: :categories_products
  has_many :products_tags, dependent: :destroy
  accepts_nested_attributes_for :products_tags, allow_destroy: true
  has_many :tags, through: :products_tags
  has_many :products_rankings, dependent: :destroy
  has_many :rankings, through: :products_rankings
  has_many :reviews, dependent: :destroy
  has_many :coordinates_products, dependent: :destroy
  has_many :coordinates, through: :coordinates_products
  has_many :recommends, dependent: :destroy
  has_many :favorites, dependent: :destroy

  has_many_attached :images

  max_paginates_per 100

  scope :category, lambda { |category_code|
    joins(:categories).merge(Category.where(code: category_code))
  }

  scope :tag, lambda { |tag_code|
    joins(:tags).merge(Tag.where(code: tag_code))
  }

  scope :ranking, lambda { |ranking_code|
    joins(:rankings).merge(Ranking.where(code: ranking_code))
  }

  def self.ransackable_scopes(_auth_object = nil)
    %i[category tag ranking]
  end

  def colors
    variations.map(&:color).uniq
  end

  def image_paths
    images.map do |image|
      URI.join(
        Rails.application.config.action_controller.asset_host,
        Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
      )
    end
  end
end
