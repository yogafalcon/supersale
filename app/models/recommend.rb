# frozen_string_literal: true

# == Schema Information
#
# Table name: recommends
#
#  id                   :bigint(8)        not null, primary key
#  category             :integer
#  product_id           :bigint(8)
#  recommend_product_id :bigint(8)
#  priority             :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Recommend < ApplicationRecord
  belongs_to :product
  belongs_to :recommend_product, class_name: 'Product'

  enum category: %i[also_bought]
end
