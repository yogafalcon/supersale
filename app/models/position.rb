# frozen_string_literal: true

# == Schema Information
#
# Table name: positions
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Position < ApplicationRecord
  has_many :staffs, dependent: :restrict_with_error
end
