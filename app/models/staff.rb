# frozen_string_literal: true

# == Schema Information
#
# Table name: staffs
#
#  id          :bigint(8)        not null, primary key
#  division_id :bigint(8)
#  position_id :bigint(8)
#  store_id    :bigint(8)
#  name        :string(255)
#  code        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Staff < ApplicationRecord
  belongs_to :division, optional: true
  belongs_to :position, optional: true
  belongs_to :store, optional: true
end
