# frozen_string_literal: true

# == Schema Information
#
# Table name: coordinates_products
#
#  id            :bigint(8)        not null, primary key
#  coordinate_id :bigint(8)
#  product_id    :bigint(8)
#  priority      :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class CoordinatesProduct < ApplicationRecord
  belongs_to :coordinate
  belongs_to :product
end
