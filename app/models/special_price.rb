# frozen_string_literal: true

# == Schema Information
#
# Table name: special_prices
#
#  id             :bigint(8)        not null, primary key
#  product_id     :bigint(8)
#  price          :integer
#  point          :integer
#  begin_datetime :datetime
#  end_datetime   :datetime
#  comment        :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class SpecialPrice < ApplicationRecord
  belongs_to :product
end
