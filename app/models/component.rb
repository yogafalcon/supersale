# frozen_string_literal: true

# == Schema Information
#
# Table name: components
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  code       :string(255)
#  content    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Component < ApplicationRecord
end
