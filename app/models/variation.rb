# frozen_string_literal: true

# == Schema Information
#
# Table name: variations
#
#  id         :bigint(8)        not null, primary key
#  product_id :bigint(8)
#  color_id   :bigint(8)
#  size_id    :bigint(8)
#  stock      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Variation < ApplicationRecord
  belongs_to :product
  belongs_to :color
  belongs_to :size
  has_many :orders_variations, dependent: :restrict_with_error
  has_many :orders, through: :orders_variations

  has_one_attached :image

  def image_path
    URI.join(
      Rails.application.config.action_controller.asset_host,
      Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
    )
  end
end
