# frozen_string_literal: true

# == Schema Information
#
# Table name: point_histories
#
#  id             :bigint(8)        not null, primary key
#  user_id        :bigint(8)
#  category       :integer
#  point          :integer
#  content        :text(65535)
#  begin_datetime :datetime
#  end_datetime   :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class PointHistory < ApplicationRecord
  belongs_to :user

  # カテゴリー: 通常、期間限定、失効
  enum category: %i[normal limited_time expired]
end
