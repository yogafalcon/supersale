# frozen_string_literal: true

# == Schema Information
#
# Table name: orders_variations
#
#  id           :bigint(8)        not null, primary key
#  order_id     :bigint(8)
#  variation_id :bigint(8)
#  quantity     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class OrdersVariation < ApplicationRecord
  belongs_to :order
  belongs_to :variation
end
