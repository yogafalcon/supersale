# frozen_string_literal: true

class Person < ApplicationRecord
  self.abstract_class = true
  def name
    "#{last_name} #{first_name}"
  end

  def kana
    "#{last_kana} #{first_kana}"
  end
end
