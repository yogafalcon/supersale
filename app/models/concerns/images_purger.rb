# frozen_string_literal: true

# use for prepend
module Concerns
  module ImagesPurger
    def self.prepended(base)
      base.attribute :_destroy_images, :boolean
    end

    # override
    def images=(attachables)
      images.purge if _destroy_images
      super attachables
    end
  end
end
