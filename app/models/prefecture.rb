# frozen_string_literal: true

# == Schema Information
#
# Table name: prefectures
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Prefecture < ApplicationRecord
  has_many :addresses, dependent: :restrict_with_error
  has_many :stores, dependent: :restrict_with_error
end
