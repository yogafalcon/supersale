# frozen_string_literal: true

# == Schema Information
#
# Table name: categories_products
#
#  id          :bigint(8)        not null, primary key
#  category_id :bigint(8)
#  product_id  :bigint(8)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class CategoriesProduct < ApplicationRecord
  belongs_to :category
  belongs_to :product
end
