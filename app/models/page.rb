# frozen_string_literal: true

# == Schema Information
#
# Table name: pages
#
#  id            :bigint(8)        not null, primary key
#  category      :integer
#  reference_url :string(255)
#  subject       :string(255)
#  content       :text(65535)
#  priority      :integer
#  enabled       :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Page < ApplicationRecord
  prepend Concerns::ImagesPurger

  enum category: %i[normal feature main_feature sub_feature key_visual]

  has_many_attached :images

  def image_paths
    images.map do |image|
      URI.join(
        Rails.application.config.action_controller.asset_host,
        Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
      )
    end
  end
end
