# frozen_string_literal: true

# == Schema Information
#
# Table name: products_rankings
#
#  id         :bigint(8)        not null, primary key
#  product_id :bigint(8)
#  ranking_id :bigint(8)
#  priority   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProductsRanking < ApplicationRecord
  belongs_to :product
  belongs_to :ranking
end
