# frozen_string_literal: true

# == Schema Information
#
# Table name: contacts
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  email      :string(255)
#  category   :integer
#  content    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Contact < ApplicationRecord
  # お問い合わせカテゴリ: 商品について、注文について
  enum category: %i[about_product about_order]

  validates :name, presence: true
  validates :email, presence: true
  validates :email, email: true
  validates :category, presence: true
  validates :category, inclusion: { in: categories }
  validates :content, presence: true
end
