# frozen_string_literal: true

# == Schema Information
#
# Table name: divisions
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Division < ApplicationRecord
  has_many :staffs, dependent: :restrict_with_error
end
