# frozen_string_literal: true

# == Schema Information
#
# Table name: orders
#
#  id             :bigint(8)        not null, primary key
#  category       :integer
#  status         :integer
#  payment_status :integer
#  user_id        :bigint(8)
#  first_name     :string(255)
#  last_name      :string(255)
#  first_kana     :string(255)
#  last_kana      :string(255)
#  postal_code    :string(255)
#  prefecture_id  :bigint(8)
#  address_1      :string(255)
#  address_2      :string(255)
#  address_3      :string(255)
#  tel_no         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Order < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :prefecture
  has_many :orders_variations, dependent: :destroy
  has_many :variations, through: :orders_variations

  enum category: %i[normal pre_order back_order]
  # 発送ステータス: 引当待ち、発送待ち、発送完了、キャンセル
  enum status: %i[waiting_allocation waiting_shipping shipped cancel]
  # 支払ステータス: 支払待ち、支払完了、返金待ち、返金完了
  enum payment_status: %i[waiting_payment paid waiting_refundment refunded]
end
