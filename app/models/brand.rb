# frozen_string_literal: true

# == Schema Information
#
# Table name: brands
#
#  id         :bigint(8)        not null, primary key
#  company_id :bigint(8)
#  name       :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Brand < ApplicationRecord
  belongs_to :company
  has_many :products, dependent: :restrict_with_error
end
