# frozen_string_literal: true

# == Schema Information
#
# Table name: stores
#
#  id            :bigint(8)        not null, primary key
#  name          :string(255)
#  code          :string(255)
#  postal_code   :string(255)
#  prefecture_id :bigint(8)
#  address_1     :string(255)
#  address_2     :string(255)
#  address_3     :string(255)
#  tel_no        :string(255)
#  blog_url      :string(255)
#  rss_url       :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Store < ApplicationRecord
  has_many :staffs, dependent: :restrict_with_error
  belongs_to :prefecture
  has_one :store_blog, dependent: :restrict_with_error

  has_one_attached :image

  def address
    "#{prefecture&.name}#{address_1}#{address_2} #{address_3}"
  end

  def image_path
    return unless image.attached?
    URI.join(
      Rails.application.config.action_controller.asset_host,
      Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
    )
  end
end
