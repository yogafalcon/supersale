# frozen_string_literal: true

# == Schema Information
#
# Table name: store_blogs
#
#  id         :bigint(8)        not null, primary key
#  store_id   :bigint(8)
#  name       :string(255)
#  subject    :string(255)
#  content    :text(65535)
#  priority   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class StoreBlog < ApplicationRecord
  belongs_to :store, optional: true

  has_one_attached :image

  def image_path
    return unless image.attached?
    URI.join(
      Rails.application.config.action_controller.asset_host,
      Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
    )
  end
end
