# frozen_string_literal: true

module ProductDecorator
  def category_list
    content_tag :ul, class: 'list-group' do
      categories.each do |category|
        concat content_tag(:li, category.name, class: 'list-group-item')
      end
    end
  end

  def tag_list
    content_tag :ul, class: 'list-group' do
      tags.each do |tag|
        concat content_tag(:li, tag.name, class: 'list-group-item')
      end
    end
  end

  def special_price_list
    content_tag :table, class: 'table table-bordered' do
      concat(content_tag(:thead) do
        concat(content_tag(:tr) do
          concat content_tag :th, '価格'
          concat content_tag :th, 'ポイント'
          concat content_tag :th, 'コメント'
          concat content_tag :th, '開始日時'
          concat content_tag :th, '終了日時'
        end)
      end)
      concat(content_tag(:tbody) do
        special_prices.each do |special_price|
          concat(content_tag(:tr) do
            concat content_tag(:td, special_price.price.to_s(:delimited))
            concat content_tag(:td, special_price.point.to_s(:delimited))
            concat content_tag(:td, special_price.comment)
            concat content_tag(:td, lp(special_price.begin_datetime))
            concat content_tag(:td, lp(special_price.end_datetime))
          end)
        end
      end)
    end
  end
end
