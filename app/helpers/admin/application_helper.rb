# frozen_string_literal: true

module Admin::ApplicationHelper
  def thumbnails(images, options = {})
    return unless images.attached?
    first_only = options.key?(:first_only) ? options[:first_only] : false
    image_tags = images.map { |image| thumbnail_tag image, options }
    safe_join first_only ? image_tags.slice(0, 1) : image_tags, ''
  end

  def thumbnail(image, options = {})
    return unless image.attached?
    thumbnail_tag image, options
  end

  # [l]ocalize if text is [p]resent
  def lp(date_text, format = :default)
    l(date_text, format: format) if date_text.present?
  end

  private

  def thumbnail_tag(image, options)
    resize = options.key?(:resize) ? options[:resize] : '160x160'
    image_tag image.variant(resize: resize), alt: 'thumbnail'
  end
end
