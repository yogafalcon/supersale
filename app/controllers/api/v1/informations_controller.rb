# frozen_string_literal: true

class Api::V1::InformationsController < Api::V1::ApplicationController
  before_action :set_information, only: %i[show]

  # GET /informations
  def index
    @informations = Information.search(params[:q]).result
                               .order(release_datetime: :desc).page(params[:page]).per(params[:per_page])

    paginate json: @informations
  end

  # GET /informations/1
  def show
    render json: @information
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_information
    @information = Information.find(params[:id])
  end
end
