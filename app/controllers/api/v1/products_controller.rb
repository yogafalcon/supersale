# frozen_string_literal: true

class Api::V1::ProductsController < Api::V1::ApplicationController
  before_action :set_product, only: %i[show]

  # GET /products
  def index
    @products = Product.includes(:tags, variations: %i[color size])
                       .with_attached_images
                       .search(params[:q]).result
                       .page(params[:page]).per(params[:per_page])

    paginate json: @products
  end

  # GET /products/1
  def show
    render json: @product
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find(params[:id])
  end
end
