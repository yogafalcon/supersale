# frozen_string_literal: true

class Api::V1::RankingsController < Api::V1::ApplicationController
  before_action :set_ranking, only: %i[show]

  # GET /rankings
  def index
    @rankings = Ranking.search(params[:q]).result.page(params[:page]).per(params[:per_page])

    paginate json: @rankings
  end

  # GET /rankings/1
  def show
    render json: @ranking
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_ranking
    @ranking = Ranking.includes(:products).find(params[:id])
  end
end
