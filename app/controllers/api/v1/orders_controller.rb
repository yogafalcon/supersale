# frozen_string_literal: true

class Api::V1::OrdersController < Api::V1::ApplicationController
  before_action :authenticate_user!
  before_action :set_order, only: [:show]

  # GET /orders
  def index
    @orders = Order.where(user: current_user)
                   .search(params[:q]).result
                   .page(params[:page]).per(params[:per_page])

    paginate json: @orders
  end

  # GET /orders/1
  def show
    render json: @order
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.where(user: current_user).find(params[:id])
  end
end
