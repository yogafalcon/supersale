# frozen_string_literal: true

class Api::V1::VariationsController < Api::V1::ApplicationController
  before_action :set_variation, only: %i[show]

  # GET /variations
  def index
    @variations = Variation.includes(:color, :size, product: { images_attachments: :blob })
                           .with_attached_image
                           .search(params[:q]).result
                           .page(params[:page]).per(params[:per_page])

    paginate json: @variations
  end

  # GET /variations/1
  def show
    render json: @variation
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_variation
    @variation = Variation.find(params[:id])
  end
end
