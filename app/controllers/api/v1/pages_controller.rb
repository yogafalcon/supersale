# frozen_string_literal: true

class Api::V1::PagesController < Api::V1::ApplicationController
  before_action :set_page, only: %i[show]

  # GET /pages
  def index
    @pages = Page.with_attached_images
                 .search(params[:q]).result
                 .page(params[:page]).per(params[:per_page])

    paginate json: @pages
  end

  # GET /pages/1
  def show
    render json: @page
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_page
    @page = Page.find(params[:id])
  end
end
