# frozen_string_literal: true

class Api::V1::StoreBlogsController < Api::V1::ApplicationController
  before_action :set_store_blog, only: %i[show]

  # GET /store_blogs
  def index
    @store_blogs = StoreBlog.includes(store: [:prefecture, image_attachment: :blob])
                            .search(params[:q]).result
                            .order(priority: :asc)
                            .page(params[:page]).per(params[:per_page])

    paginate json: @store_blogs
  end

  # GET /store_blogs/1
  def show
    render json: @store_blog
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_store_blog
    @store_blog = StoreBlog.includes(store: [:prefecture, image_attachment: :blob])
                           .find(params[:id])
  end
end
