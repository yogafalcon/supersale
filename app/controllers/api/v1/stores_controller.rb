# frozen_string_literal: true

class Api::V1::StoresController < Api::V1::ApplicationController
  before_action :set_store, only: %i[show]

  # GET /stores
  def index
    @stores = Store.includes(:prefecture)
                   .with_attached_image
                   .search(params[:q]).result
                   .page(params[:page]).per(params[:per_page])

    paginate json: @stores
  end

  # GET /stores/1
  def show
    render json: @store
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_store
    @store = Store.find(params[:id])
  end
end
