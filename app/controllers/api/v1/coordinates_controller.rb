# frozen_string_literal: true

class Api::V1::CoordinatesController < Api::V1::ApplicationController
  before_action :set_coordinate, only: %i[show]

  # GET /coordinates
  def index
    @coordinates = Coordinate.includes(:staff)
                             .with_attached_images
                             .search(params[:q]).result
                             .page(params[:page]).per(params[:per_page])

    paginate json: @coordinates
  end

  # GET /coordinates/1
  def show
    render json: @coordinate
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_coordinate
    @coordinate = Coordinate.find(params[:id])
  end
end
