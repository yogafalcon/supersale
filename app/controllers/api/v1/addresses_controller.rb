# frozen_string_literal: true

class Api::V1::AddressesController < Api::V1::ApplicationController
  before_action :authenticate_user!
  before_action :set_address, only: %i[show update destroy]

  # GET /addresses
  def index
    @addresses = Address.where(user: current_user)
                        .search(params[:q]).result
                        .page(params[:page]).per(params[:per_page])

    paginate json: @addresses
  end

  # GET /addresses/1
  def show
    render json: @address
  end

  # POST /addresses
  def create
    @address = Address.new(address_params)

    if @address.save
      render json: @address, status: :created
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /addresses/1
  def update
    if @address.update(address_params)
      render json: @address
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # DELETE /addresses/1
  def destroy
    @address.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_address
    @address = Address.where(user: current_user).find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def address_params
    params.require(:address).permit(:user_id, :last_name, :first_name, :last_kana, :first_kana,
                                    :postal_code, :prefecture_id, :address_1, :address_2, :address_3, :tel_no)
  end
end
