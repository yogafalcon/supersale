# frozen_string_literal: true

class Api::ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
end
