# frozen_string_literal: true

# only write about devise in this controller.
class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session, if: :verify_api
  layout :layout_by_resource

  private

  def verify_api
    devise_controller? && resource_name == :user
  end

  def layout_by_resource
    return 'application' unless devise_controller?

    if resource_name == :admin
      prepend_view_path 'app/views/admin'
      'admin/layouts/application'
    else
      'application'
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    if resource_or_scope == :admin
      admin_root_path
    else
      root_path
    end
  end
end
