# frozen_string_literal: true

class Admin::OrdersController < Admin::ApplicationController
  before_action :set_order, only: %i[show edit update destroy]

  # GET /orders
  def index
    @q = Order.includes(:user, :prefecture).search(params[:q])
    authorize @orders = @q.result.page(params[:page])
  end

  # GET /orders/1
  def show; end

  # GET /orders/new
  def new
    authorize @order = Order.new
  end

  # GET /orders/1/edit
  def edit; end

  # POST /orders
  def create
    authorize @order = Order.new(order_params)

    if @order.save
      redirect_to [:admin, @order], notice: t('notice.create.success', name: Order.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(order_params)
      redirect_to [:admin, @order], notice: t('notice.update.success', name: Order.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /orders/1
  def destroy
    @order.destroy
    redirect_to admin_orders_url, notice: t('notice.destroy.success', name: Order.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    authorize @order = Order.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def order_params
    params.require(:order).permit(:category, :status, :payment_status, :user_id,
                                  :last_name, :first_name, :last_kana, :first_kana, :postal_code,
                                  :prefecture_id, :address_1, :address_2, :address_3, :tel_no)
  end
end
