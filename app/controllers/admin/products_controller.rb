# frozen_string_literal: true

class Admin::ProductsController < Admin::ApplicationController
  before_action :set_product, only: %i[show edit update destroy]

  # GET /products
  def index
    @q = Product.includes(:brand, :categories_products, :categories, :products_tags, :tags)
                .with_attached_images.search(params[:q])
    authorize @products = @q.result.page(params[:page])
  end

  # GET /products/1
  def show; end

  # GET /products/new
  def new
    authorize @product = Product.new
  end

  # GET /products/1/edit
  def edit; end

  # POST /products
  def create
    authorize @product = Product.new(product_params)

    if @product.save
      redirect_to [:admin, @product], notice: t('notice.create.success', name: Product.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      redirect_to [:admin, @product], notice: t('notice.update.success', name: Product.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
    redirect_to admin_products_url, notice: t('notice.destroy.success', name: Product.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_product
    authorize @product = Product.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def product_params
    params.require(:product).permit(
      :brand_id, :name, :code, :caption, :description, :price,
      :point, :cost, :priority, :begin_datetime, :end_datetime, :material,
      :country_of_origin, :favorites_count, :enabled,
      :_destroy_images,
      images: [],
      categories_products_attributes: %i[id _destroy category_id],
      products_tags_attributes: %i[id _destroy tag_id],
      special_prices_attributes: %i[id _destroy price point comment begin_datetime end_datetime]
    )
  end
end
