# frozen_string_literal: true

class Admin::TagsController < Admin::ApplicationController
  before_action :set_tag, only: %i[show edit update destroy]

  # GET /tags
  def index
    @q = Tag.search(params[:q])
    authorize @tags = @q.result.page(params[:page])
  end

  # GET /tags/1
  def show; end

  # GET /tags/new
  def new
    authorize @tag = Tag.new
  end

  # GET /tags/1/edit
  def edit; end

  # POST /tags
  def create
    authorize @tag = Tag.new(tag_params)

    if @tag.save
      redirect_to [:admin, @tag], notice: t('notice.create.success', name: Tag.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /tags/1
  def update
    if @tag.update(tag_params)
      redirect_to [:admin, @tag], notice: t('notice.update.success', name: Tag.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /tags/1
  def destroy
    @tag.destroy
    redirect_to admin_tags_url, notice: t('notice.destroy.success', name: Tag.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_tag
    authorize @tag = Tag.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def tag_params
    params.require(:tag).permit(:name, :code)
  end
end
