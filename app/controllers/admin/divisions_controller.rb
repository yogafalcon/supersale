# frozen_string_literal: true

class Admin::DivisionsController < Admin::ApplicationController
  before_action :set_division, only: %i[show edit update destroy]

  # GET /divisions
  def index
    @q = Division.search(params[:q])
    authorize @divisions = @q.result.page(params[:page])
  end

  # GET /divisions/1
  def show; end

  # GET /divisions/new
  def new
    authorize @division = Division.new
  end

  # GET /divisions/1/edit
  def edit; end

  # POST /divisions
  def create
    authorize @division = Division.new(division_params)

    if @division.save
      redirect_to [:admin, @division], notice: t('notice.create.success', name: Division.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /divisions/1
  def update
    if @division.update(division_params)
      redirect_to [:admin, @division], notice: t('notice.update.success', name: Division.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /divisions/1
  def destroy
    @division.destroy
    redirect_to admin_divisions_url, notice: t('notice.destroy.success', name: Division.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_division
    authorize @division = Division.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def division_params
    params.require(:division).permit(:name, :code)
  end
end
