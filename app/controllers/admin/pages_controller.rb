# frozen_string_literal: true

class Admin::PagesController < Admin::ApplicationController
  before_action :set_page, only: %i[show edit update destroy]

  # GET /pages
  def index
    @q = Page.with_attached_images.search(params[:q])
    authorize @pages = @q.result.page(params[:page])
  end

  # GET /pages/1
  def show; end

  # GET /pages/new
  def new
    authorize @page = Page.new
  end

  # GET /pages/1/edit
  def edit; end

  # POST /pages
  def create
    authorize @page = Page.new(page_params)

    if @page.save
      redirect_to [:admin, @page], notice: t('notice.create.success', name: Page.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /pages/1
  def update
    if @page.update(page_params)
      redirect_to [:admin, @page], notice: t('notice.update.success', name: Page.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /pages/1
  def destroy
    @page.destroy
    redirect_to admin_pages_url, notice: t('notice.destroy.success', name: Page.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_page
    authorize @page = Page.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def page_params
    params.require(:page).permit(:category, :reference_url, :subject, :content, :priority, :enabled,
                                 :_destroy_images, images: [])
  end
end
