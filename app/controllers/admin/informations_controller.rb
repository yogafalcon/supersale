# frozen_string_literal: true

class Admin::InformationsController < Admin::ApplicationController
  before_action :set_information, only: %i[show edit update destroy]

  # GET /informations
  def index
    @q = Information.search(params[:q])
    authorize @informations = @q.result.page(params[:page])
  end

  # GET /informations/1
  def show; end

  # GET /informations/new
  def new
    authorize @information = Information.new
  end

  # GET /informations/1/edit
  def edit; end

  # POST /informations
  def create
    authorize @information = Information.new(information_params)

    if @information.save
      redirect_to [:admin, @information], notice: t('notice.create.success', name: Information.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /informations/1
  def update
    if @information.update(information_params)
      redirect_to [:admin, @information], notice: t('notice.update.success', name: Information.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /informations/1
  def destroy
    @information.destroy
    redirect_to admin_informations_url, notice: t('notice.destroy.success', name: Information.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_information
    authorize @information = Information.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def information_params
    params.require(:information).permit(:release_datetime, :subject, :content)
  end
end
