# frozen_string_literal: true

class Admin::CategoriesController < Admin::ApplicationController
  before_action :set_category, only: %i[show edit update destroy]

  # GET /categories
  def index
    @q = Category.search(params[:q])
    authorize @categories = @q.result.page(params[:page])
  end

  # GET /categories/1
  def show; end

  # GET /categories/new
  def new
    authorize @category = Category.new
  end

  # GET /categories/1/edit
  def edit; end

  # POST /categories
  def create
    authorize @category = Category.new(category_params)

    if @category.save
      redirect_to [:admin, @category], notice: t('notice.create.success', name: Category.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /categories/1
  def update
    if @category.update(category_params)
      redirect_to [:admin, @category], notice: t('notice.update.success', name: Category.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /categories/1
  def destroy
    @category.destroy
    redirect_to admin_categories_url, notice: t('notice.destroy.success', name: Category.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_category
    authorize @category = Category.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def category_params
    params.require(:category).permit(:name, :code)
  end
end
