# frozen_string_literal: true

class Admin::BrandsController < Admin::ApplicationController
  before_action :set_brand, only: %i[show edit update destroy]

  # GET /brands
  def index
    @q = Brand.includes(:company).search(params[:q])
    authorize @brands = @q.result.page(params[:page])
  end

  # GET /brands/1
  def show; end

  # GET /brands/new
  def new
    authorize @brand = Brand.new
  end

  # GET /brands/1/edit
  def edit; end

  # POST /brands
  def create
    authorize @brand = Brand.new(brand_params)

    if @brand.save
      redirect_to [:admin, @brand], notice: t('notice.create.success', name: Brand.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /brands/1
  def update
    if @brand.update(brand_params)
      redirect_to [:admin, @brand], notice: t('notice.update.success', name: Brand.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /brands/1
  def destroy
    @brand.destroy
    redirect_to admin_brands_url, notice: t('notice.destroy.success', name: Brand.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_brand
    authorize @brand = Brand.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def brand_params
    params.require(:brand).permit(:company_id, :name, :code)
  end
end
