# frozen_string_literal: true

class Admin::StoreBlogsController < Admin::ApplicationController
  before_action :set_store_blog, only: %i[show edit update destroy]

  # GET /store_blogs
  def index
    @q = StoreBlog.includes(:store).with_attached_image.search(params[:q])
    authorize @store_blogs = @q.result.page(params[:page])
  end

  # GET /store_blogs/1
  def show; end

  # GET /store_blogs/new
  def new
    authorize @store_blog = StoreBlog.new
  end

  # GET /store_blogs/1/edit
  def edit; end

  # POST /store_blogs
  def create
    authorize @store_blog = StoreBlog.new(store_blog_params)

    if @store_blog.save
      redirect_to [:admin, @store_blog], notice: t('notice.create.success', name: StoreBlog.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /store_blogs/1
  def update
    if @store_blog.update(store_blog_params)
      redirect_to [:admin, @store_blog], notice: t('notice.update.success', name: StoreBlog.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /store_blogs/1
  def destroy
    @store_blog.destroy
    redirect_to admin_store_blogs_url, notice: t('notice.destroy.success', name: StoreBlog.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_store_blog
    authorize @store_blog = StoreBlog.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def store_blog_params
    params.require(:store_blog).permit(:store_id, :name, :subject, :content, :priority, :image)
  end
end
