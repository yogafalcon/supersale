# frozen_string_literal: true

class Admin::RankingsController < Admin::ApplicationController
  before_action :set_ranking, only: %i[show edit update destroy]

  # GET /rankings
  def index
    @q = Ranking.search(params[:q])
    authorize @rankings = @q.result.page(params[:page])
  end

  # GET /rankings/1
  def show; end

  # GET /rankings/new
  def new
    authorize @ranking = Ranking.new
  end

  # GET /rankings/1/edit
  def edit; end

  # POST /rankings
  def create
    authorize @ranking = Ranking.new(ranking_params)

    if @ranking.save
      redirect_to [:admin, @ranking], notice: t('notice.create.success', name: Ranking.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /rankings/1
  def update
    if @ranking.update(ranking_params)
      redirect_to [:admin, @ranking], notice: t('notice.update.success', name: Ranking.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /rankings/1
  def destroy
    @ranking.destroy
    redirect_to admin_rankings_url, notice: t('notice.destroy.success', name: Ranking.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_ranking
    authorize @ranking = Ranking.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def ranking_params
    params.require(:ranking).permit(:name, :code)
  end
end
