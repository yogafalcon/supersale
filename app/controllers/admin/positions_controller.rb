# frozen_string_literal: true

class Admin::PositionsController < Admin::ApplicationController
  before_action :set_position, only: %i[show edit update destroy]

  # GET /positions
  def index
    @q = Position.search(params[:q])
    authorize @positions = @q.result.page(params[:page])
  end

  # GET /positions/1
  def show; end

  # GET /positions/new
  def new
    authorize @position = Position.new
  end

  # GET /positions/1/edit
  def edit; end

  # POST /positions
  def create
    authorize @position = Position.new(position_params)

    if @position.save
      redirect_to [:admin, @position], notice: t('notice.create.success', name: Position.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /positions/1
  def update
    if @position.update(position_params)
      redirect_to [:admin, @position], notice: t('notice.update.success', name: Position.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /positions/1
  def destroy
    @position.destroy
    redirect_to admin_positions_url, notice: t('notice.destroy.success', name: Position.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_position
    authorize @position = Position.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def position_params
    params.require(:position).permit(:name, :code)
  end
end
