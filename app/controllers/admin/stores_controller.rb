# frozen_string_literal: true

class Admin::StoresController < Admin::ApplicationController
  before_action :set_store, only: %i[show edit update destroy]

  # GET /stores
  def index
    @q = Store.includes(:prefecture).with_attached_image.search(params[:q])
    authorize @stores = @q.result.page(params[:page])
  end

  # GET /stores/1
  def show; end

  # GET /stores/new
  def new
    authorize @store = Store.new
  end

  # GET /stores/1/edit
  def edit; end

  # POST /stores
  def create
    authorize @store = Store.new(store_params)

    if @store.save
      redirect_to [:admin, @store], notice: t('notice.create.success', name: Store.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /stores/1
  def update
    if @store.update(store_params)
      redirect_to [:admin, @store], notice: t('notice.update.success', name: Store.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /stores/1
  def destroy
    @store.destroy
    redirect_to admin_stores_url, notice: t('notice.destroy.success', name: Store.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_store
    authorize @store = Store.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def store_params
    params.require(:store).permit(:name, :code, :postal_code, :prefecture_id,
                                  :address_1, :address_2, :address_3, :tel_no, :blog_url, :rss_url, :image)
  end
end
