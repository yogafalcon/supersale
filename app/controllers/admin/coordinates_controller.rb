# frozen_string_literal: true

class Admin::CoordinatesController < Admin::ApplicationController
  before_action :set_coordinate, only: %i[show edit update destroy]

  # GET /coordinates
  def index
    @q = Coordinate.search(params[:q])
    authorize @coordinates = @q.result.page(params[:page])
  end

  # GET /coordinates/1
  def show; end

  # GET /coordinates/new
  def new
    authorize @coordinate = Coordinate.new
  end

  # GET /coordinates/1/edit
  def edit; end

  # POST /coordinates
  def create
    authorize @coordinate = Coordinate.new(coordinate_params)

    if @coordinate.save
      redirect_to [:admin, @coordinate], notice: t('notice.create.success', name: Coordinate.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /coordinates/1
  def update
    if @coordinate.update(coordinate_params)
      redirect_to [:admin, @coordinate], notice: t('notice.update.success', name: Coordinate.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /coordinates/1
  def destroy
    @coordinate.destroy
    redirect_to admin_coordinates_url, notice: t('notice.destroy.success', name: Coordinate.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_coordinate
    authorize @coordinate = Coordinate.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def coordinate_params
    params.require(:coordinate).permit(:staff_id, :point_1, :point_2, :point_3, :comment)
  end
end
