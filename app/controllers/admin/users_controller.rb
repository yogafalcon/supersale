# frozen_string_literal: true

class Admin::UsersController < Admin::ApplicationController
  before_action :set_user, only: %i[show edit update destroy]

  # GET /users
  def index
    @q = User.search(params[:q])
    authorize @users = @q.result.page(params[:page])
  end

  # GET /users/1
  def show; end

  # GET /users/new
  def new
    authorize @user = User.new
  end

  # GET /users/1/edit
  def edit; end

  # POST /users
  def create
    authorize @user = User.new(user_params)

    if @user.save
      redirect_to [:admin, @user], notice: t('notice.create.success', name: User.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      redirect_to [:admin, @user], notice: t('notice.update.success', name: User.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to admin_users_url, notice: t('notice.destroy.success', name: User.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    authorize @user = User.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def user_params
    params.require(:user).permit(:nickname, :email, :birthday, :sex)
  end
end
