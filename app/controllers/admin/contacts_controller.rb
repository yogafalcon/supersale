# frozen_string_literal: true

class Admin::ContactsController < Admin::ApplicationController
  before_action :set_contact, only: %i[show edit update destroy]

  # GET /contacts
  def index
    @q = Contact.search(params[:q])
    authorize @contacts = @q.result.page(params[:page]).order(created_at: :desc)
  end

  # GET /contacts/1
  def show; end

  # GET /contacts/new
  def new
    authorize @contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit; end

  # POST /contacts
  def create
    authorize @contact = Contact.new(contact_params)

    if @contact.save
      redirect_to [:admin, @contact], notice: t('notice.create.success', name: Contact.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /contacts/1
  def update
    if @contact.update(contact_params)
      redirect_to [:admin, @contact], notice: t('notice.update.success', name: Contact.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /contacts/1
  def destroy
    @contact.destroy
    redirect_to admin_contacts_url, notice: t('notice.destroy.success', name: Contact.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_contact
    authorize @contact = Contact.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def contact_params
    params.require(:contact).permit(:name, :email, :category, :content)
  end
end
