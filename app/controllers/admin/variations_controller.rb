# frozen_string_literal: true

class Admin::VariationsController < Admin::ApplicationController
  before_action :set_variation, only: %i[show edit update destroy]

  # GET /variations
  def index
    @q = Variation.includes(:product, :color, :size)
                  .with_attached_image.search(params[:q])
    authorize @variations = @q.result.page(params[:page])
  end

  # GET /variations/1
  def show; end

  # GET /variations/new
  def new
    authorize @variation = Variation.new
  end

  # GET /variations/1/edit
  def edit; end

  # POST /variations
  def create
    authorize @variation = Variation.new(variation_params)

    if @variation.save
      redirect_to [:admin, @variation], notice: t('notice.create.success', name: Variation.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /variations/1
  def update
    if @variation.update(variation_params)
      redirect_to [:admin, @variation], notice: t('notice.update.success', name: Variation.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /variations/1
  def destroy
    @variation.destroy
    redirect_to admin_variations_url, notice: t('notice.destroy.success', name: Variation.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_variation
    authorize @variation = Variation.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def variation_params
    params.require(:variation).permit(:product_id, :color_id, :size_id, :stock, :image)
  end
end
