# frozen_string_literal: true

class Admin::StaffsController < Admin::ApplicationController
  before_action :set_staff, only: %i[show edit update destroy]

  # GET /staffs
  def index
    @q = Staff.includes(:store, :division, :position).search(params[:q])
    authorize @staffs = @q.result.page(params[:page])
  end

  # GET /staffs/1
  def show; end

  # GET /staffs/new
  def new
    authorize @staff = Staff.new
  end

  # GET /staffs/1/edit
  def edit; end

  # POST /staffs
  def create
    authorize @staff = Staff.new(staff_params)

    if @staff.save
      redirect_to [:admin, @staff], notice: t('notice.create.success', name: Staff.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /staffs/1
  def update
    if @staff.update(staff_params)
      redirect_to [:admin, @staff], notice: t('notice.update.success', name: Staff.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /staffs/1
  def destroy
    @staff.destroy
    redirect_to admin_staffs_url, notice: t('notice.destroy.success', name: Staff.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_staff
    authorize @staff = Staff.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def staff_params
    params.require(:staff).permit(:store_id, :division_id, :position_id, :name, :code)
  end
end
