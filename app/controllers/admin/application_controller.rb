# frozen_string_literal: true

class Admin::ApplicationController < ApplicationController
  include Pundit
  layout 'admin/layouts/application'

  before_action :authenticate_admin!

  prepend_view_path 'app/views/admin'

  def pundit_user
    current_admin
  end
end
