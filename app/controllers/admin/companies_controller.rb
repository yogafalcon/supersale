# frozen_string_literal: true

class Admin::CompaniesController < Admin::ApplicationController
  before_action :set_company, only: %i[show edit update destroy]

  # GET /companies
  def index
    @q = Company.search(params[:q])
    authorize @companies = @q.result.page(params[:page])
  end

  # GET /companies/1
  def show; end

  # GET /companies/new
  def new
    authorize @company = Company.new
  end

  # GET /companies/1/edit
  def edit; end

  # POST /companies
  def create
    authorize @company = Company.new(company_params)

    if @company.save
      redirect_to [:admin, @company], notice: t('notice.create.success', name: Company.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /companies/1
  def update
    if @company.update(company_params)
      redirect_to [:admin, @company], notice: t('notice.update.success', name: Company.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /companies/1
  def destroy
    @company.destroy
    redirect_to admin_companies_url, notice: t('notice.destroy.success', name: Company.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_company
    authorize @company = Company.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def company_params
    params.require(:company).permit(:name, :code)
  end
end
