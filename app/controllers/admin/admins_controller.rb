# frozen_string_literal: true

class Admin::AdminsController < Admin::ApplicationController
  before_action :set_admin, only: %i[show edit update destroy]

  # GET /admins
  def index
    @q = Admin.with_attached_avatar.search(params[:q])
    authorize @admins = @q.result.page(params[:page])
  end

  # GET /admins/1
  def show; end

  # GET /admins/new
  def new
    authorize @admin = Admin.new
  end

  # GET /admins/1/edit
  def edit; end

  # POST /admins
  def create
    authorize @admin = Admin.new(admin_params)

    if @admin.save
      redirect_to [:admin, @admin], notice: t('notice.create.success', name: Admin.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /admins/1
  def update
    if @admin.update(admin_params)
      redirect_to [:admin, @admin], notice: t('notice.update.success', name: Admin.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /admins/1
  def destroy
    @admin.destroy
    redirect_to admin_admins_url, notice: t('notice.destroy.success', name: Admin.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_admin
    authorize @admin = Admin.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def admin_params
    params.require(:admin).permit(:email, :password, :avatar)
  end
end
