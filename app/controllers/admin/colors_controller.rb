# frozen_string_literal: true

class Admin::ColorsController < Admin::ApplicationController
  before_action :set_color, only: %i[show edit update destroy]

  # GET /colors
  def index
    @q = Color.search(params[:q])
    authorize @colors = @q.result.page(params[:page])
  end

  # GET /colors/1
  def show; end

  # GET /colors/new
  def new
    authorize @color = Color.new
  end

  # GET /colors/1/edit
  def edit; end

  # POST /colors
  def create
    authorize @color = Color.new(color_params)

    if @color.save
      redirect_to [:admin, @color], notice: t('notice.create.success', name: Color.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /colors/1
  def update
    if @color.update(color_params)
      redirect_to [:admin, @color], notice: t('notice.update.success', name: Color.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /colors/1
  def destroy
    @color.destroy
    redirect_to admin_colors_url, notice: t('notice.destroy.success', name: Color.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_color
    authorize @color = Color.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def color_params
    params.require(:color).permit(:name, :code, :hex_code)
  end
end
