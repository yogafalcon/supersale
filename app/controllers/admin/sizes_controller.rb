# frozen_string_literal: true

class Admin::SizesController < Admin::ApplicationController
  before_action :set_size, only: %i[show edit update destroy]

  # GET /sizes
  def index
    @q = Size.search(params[:q])
    authorize @sizes = @q.result.page(params[:page])
  end

  # GET /sizes/1
  def show; end

  # GET /sizes/new
  def new
    authorize @size = Size.new
  end

  # GET /sizes/1/edit
  def edit; end

  # POST /sizes
  def create
    authorize @size = Size.new(size_params)

    if @size.save
      redirect_to [:admin, @size], notice: t('notice.create.success', name: Size.model_name.human)
    else
      render :new
    end
  end

  # PATCH/PUT /sizes/1
  def update
    if @size.update(size_params)
      redirect_to [:admin, @size], notice: t('notice.update.success', name: Size.model_name.human)
    else
      render :edit
    end
  end

  # DELETE /sizes/1
  def destroy
    @size.destroy
    redirect_to admin_sizes_url, notice: t('notice.destroy.success', name: Size.model_name.human)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_size
    authorize @size = Size.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def size_params
    params.require(:size).permit(:name, :code)
  end
end
