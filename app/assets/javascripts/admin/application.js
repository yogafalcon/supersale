// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery/dist/jquery.min.js
//= require bootstrap/dist/js/bootstrap.min.js
//= require admin-lte/dist/js/adminlte.min.js
//= require cocoon
//= require select2
//= require_tree .

// https://github.com/almasaeed2010/AdminLTE/issues/1667
// https://github.com/almasaeed2010/AdminLTE/issues/1482
var ready = function() {
  var url = window.location;

  $('ul.nav-sidebar a').filter(function() {
     return this.href == url;
  }).addClass('active');

  $('ul.nav-sidebar ul.nav-treeview a').filter(function() {
     return this.href == url;
  }).parentsUntil('.has_treeview > a').addClass('menu-open');

  $('.select2').select2({
    theme: 'bootstrap4',
    width: '100%'
  })

  return $(window).trigger('resize');
};
document.addEventListener('turbolinks:load', ready);
