# Category
key = %i[name code]
values = [
  ['PICKUP', 'pickup'],
  ['NEW ITEMS', 'new'],
  ['RE ARRIVAL', 're_arrival'],
  ['COMING SOON', 'coming_soon'],
  ['SALE', 'sale'],
  ['OUTER', 'outer'],
  ['TOPS', 'tops'],
  ['SKIRT', 'skirt'],
  ['PANTS', 'pants'],
  ['ONE-PIECE', 'one_piece'],
  ['BAG', 'bag'],
  ['SHOES', 'shoes'],
  ['FASHION ACCESSORIES', 'accessories'],
]
values.each { |value| Category.create([key, value].transpose.to_h) }
p "created #{Category.count} Category records."
