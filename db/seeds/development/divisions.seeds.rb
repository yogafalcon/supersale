# Division
4.times do
  fake = Faker::Job.field
  Division.find_or_create_by(code: fake.gsub(/ /, '_').underscore) do |division|
    division.name = fake
  end
end
p "created #{Division.count} Division records."
