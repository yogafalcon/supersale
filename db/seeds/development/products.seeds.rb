after 'development:brands' do
  # Product
  200.times do
    Product.find_or_create_by(code: Faker::Code.nric) do |product|
      product.brand = Brand.order('rand()').first
      product.name = Faker::Commerce.product_name
      product.caption = "<h5>#{Faker::Lorem.sentence}</h5>"
      product.description = Faker::Lorem.paragraphs.join('<br>')
      product.price = Random.rand(2000..12000).truncate(-2)
      product.point = (product.price * 0.02).to_i
      product.cost = (product.price * 0.8).to_i
      product.priority = Random.rand(0..100)
      product.begin_datetime = Random.rand(1..5).months.ago
      product.end_datetime = product.begin_datetime + Random.rand(3..10).months
      product.material = Faker::Commerce.material
      product.country_of_origin = Faker::Address.country
      product.enabled = Random.rand(0..9) > 0
      images = []
      4.times do
        images << { io: File.open(Rails.root.join("db/seeds/development/images/product/product_0#{Random.rand(1..4)}.jpg")), filename: 'product.jpg' }
      end
      product.images.attach(images)
    end
  end
  p "created #{Product.count} Product records."
end
