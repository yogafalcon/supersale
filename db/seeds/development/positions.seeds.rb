# Position
4.times do
  fake = Faker::Job.position
  Position.find_or_create_by(code: fake.gsub(/ /, '_').underscore) do |position|
    position.name = fake
  end
end
p "created #{Position.count} Position records."
