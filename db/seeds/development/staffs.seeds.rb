after 'development:divisions', 'development:positions', 'development:stores' do
  # Staff
  20.times do
    Staff.find_or_create_by(code: Random.rand(1..1000).to_s.rjust(8, '0')) do |employee|
      employee.name = Faker::Name.name
      employee.division = Division.order('rand()').first
      employee.position = Position.order('rand()').first
      employee.store = Store.order('rand()').first
    end
  end
  p "created #{Staff.count} Staff records."
end
