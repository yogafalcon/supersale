# Color
10.times do
  fake = Faker::Color.color_name
  Color.find_or_create_by(code: fake.gsub(/ /, '_').underscore) do |color|
    color.name = fake
    color.hex_code = Faker::Color.hex_color
  end
end
p "created #{Color.count} Color records."
