after 'development:colors', 'development:products', 'development:sizes' do
  # Variation
  Product.all.each do |product|
    Color.order('rand()').limit(3).each do |color|
      Size.order('rand()').limit(2).each do |size|
        Variation.create do |variation|
          variation.product = product
          variation.color = color
          variation.size = size
          variation.stock = Random.rand(0..20)
          variation.image.attach(io: File.open(Rails.root.join("db/seeds/development/images/product/variation_0#{Random.rand(1..4)}.jpg")), filename: 'variation.jpg')
        end
      end
    end
  end
  p "created #{Variation.count} Variation records."
end
