# Information
20.times do
  Information.create do |information|
    information.release_datetime = Random.rand(-1..10).months.ago
    information.subject = Faker::Lorem.sentence
    information.content = Faker::Lorem.paragraphs.join('<br>')
  end
end
p "created #{Information.count} Information records."
