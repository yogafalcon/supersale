# Store
after 'prefectures' do
  10.times do
    fake = Faker::Address.state
    Store.find_or_create_by(code: fake.gsub(/(, | & | )/, '_').underscore) do |store|
      store.name = fake
      store.postal_code = "#{sprintf('%03d', Random.rand(1..999))}-#{sprintf('%04d', Random.rand(1..9999))}"
      store.prefecture_id = [21,22,23,24].sample
      store.address_1 = %w[名古屋市 豊田市 豊橋市 岐阜市 浜松市].sample
      store.address_2 = Faker::Address.street_address
      store.address_3 = Faker::Address.secondary_address
      store.tel_no = Faker::PhoneNumber.cell_phone
      store.blog_url = 'https://ameblo.jp/onesb-sakae/'
      store.rss_url = 'http://rssblog.ameba.jp/onesb-sakae/rss.html'
      store.image.attach(io: File.open(Rails.root.join("db/seeds/development/images/store/store_0#{Random.rand(1..4)}.jpg")), filename: 'store.jpg')
    end
  end
end
p "created #{Store.count} Store records."
