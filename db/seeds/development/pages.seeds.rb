# Page
10.times do
  product = Product.order('rand()').first
  Page.create do |page|
    page.category = Page.categories.keys.sample
    page.subject = "#{product.name} 特集"
    page.reference_url = "/products/#{product.id}"
    page.enabled = true
  end
end

content = <<~EOF
  <div>
    <h2><%= page.subject %></h2>
    <coordinates ids="[1,2]" />
    <coordinates ids="[3,4]" />
  </div>
EOF
Page.create do |page|
  page.category = Page.categories.keys.sample
  page.subject = Faker::Lorem.sentence
  page.content = content
  page.priority = 1
  page.enabled = true
end

4.times do
  content = <<~EOF
    <div>
      <% page.images.each do |image| %>
      <%= image_tag image, alt: page.subject %>
      <% end %>
    </div>
  EOF
  Page.create do |page|
    page.category = Page.categories.keys.sample
    page.subject = Faker::Lorem.sentence
    page.content = content
    page.priority = 1
    page.enabled = true
    images = []
    Random.rand(2..4).times do
      images << { io: File.open(Rails.root.join("db/seeds/development/images/feature/feature_0#{Random.rand(1..4)}.jpg")), filename: 'feature.jpg' }
    end
    page.images.attach(images)
  end
end

p "created #{Page.count} Page records."
