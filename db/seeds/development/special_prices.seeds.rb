after 'development:products' do
  # SpecialPrice
  Product.all.each do |product|
    SpecialPrice.create do |special_price|
      special_price.product =  product
      special_price.price = (product.price * 0.9).to_i
      special_price.point = (special_price.price * 0.02).to_i
      special_price.begin_datetime = Faker::Date.between(product.begin_datetime, product.end_datetime)
      special_price.end_datetime = special_price.begin_datetime + 10.days
    end
  end
  p "created #{SpecialPrice.count} SpecialPrice records."
end
