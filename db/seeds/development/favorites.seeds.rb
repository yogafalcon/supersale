after 'development:users', 'development:products' do
  # Favorite
  User.all.each do |user|
    Product.order('rand()').limit(Random.rand(1..5)).each do |product|
      product.favorites.create(user: user)
    end
  end
  p "created #{Favorite.count} Favorite records."
end
