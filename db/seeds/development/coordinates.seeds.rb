after 'development:staffs' do
  # Coordinate
  20.times do
    Coordinate.create do |coordinate|
      coordinate.point_1 = Faker::Lorem.sentence
      coordinate.point_2 = Faker::Lorem.sentence
      coordinate.point_3 = Faker::Lorem.sentence
      coordinate.comment = Faker::Lorem.paragraphs.join('<br>')
      images = []
      Random.rand(1..2).times do
        images << { io: File.open(Rails.root.join("db/seeds/development/images/coordinate/coordinate_0#{Random.rand(1..4)}.jpg")), filename: 'coordinate.jpg' }
      end
      coordinate.images.attach(images)
    end
  end
  Staff.order('rand()').limit(10).each do |staff|
    10.times do
      Coordinate.create do |coordinate|
        coordinate.staff = staff
        coordinate.point_1 = Faker::Lorem.sentence
        coordinate.point_2 = Faker::Lorem.sentence
        coordinate.point_3 = Faker::Lorem.sentence
        coordinate.comment = Faker::Lorem.paragraphs.join('<br>')
        images = []
        Random.rand(1..2).times do
          images << { io: File.open(Rails.root.join("db/seeds/development/images/coordinate/coordinate_0#{Random.rand(1..4)}.jpg")), filename: 'coordinate.jpg' }
        end
        coordinate.images.attach(images)
      end
    end
  end
  p "created #{Coordinate.count} Coordinate records."
end
