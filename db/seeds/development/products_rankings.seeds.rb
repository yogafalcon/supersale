after 'development:products', 'development:rankings' do
  # ProductsRanking
  Ranking.all.each do |ranking|
    Product.order('rand()').limit(10).each.with_index(1) do |product, index|
      ProductsRanking.create(ranking: ranking, product: product, priority: index)
    end
  end
  p "created #{ProductsRanking.count} ProductsRanking records."
end
