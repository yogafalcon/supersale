# Company
2.times do
  fake = Faker::Company.name
  Company.find_or_create_by(code: fake.gsub(/(, | & | )/, '_').underscore) do |company|
    company.name = fake
  end
end
p "created #{Company.count} Company records."
