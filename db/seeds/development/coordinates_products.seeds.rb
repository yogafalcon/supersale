after 'development:coordinates' do
  # CoordinatesProduct
  Coordinate.all.each do |coordinate|
    Product.order('rand()').limit(Random.rand(3..4)).each.with_index(1) do |product, index|
      CoordinatesProduct.create do |coordinates_product|
        coordinates_product.coordinate = coordinate
        coordinates_product.product = product
        coordinates_product.priority = index
      end
    end
  end
  p "created #{CoordinatesProduct.count} CoordinatesProduct records."
end
