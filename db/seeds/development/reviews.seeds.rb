after 'development:users', 'development:products' do
  # Review
  Product.order('rand()').limit(30).each do |product|
    User.order('rand()').limit(Random.rand(3..10)).each do |user|
      Review.create do |review|
        review.product = product
        review.user = user
        review.rating = Random.rand(1..5)
        review.comment = Faker::Lorem.paragraphs.join("\n")
      end
    end
  end
  p "created #{Review.count} Review records."
end
