# Component
content = <<~EOF
  <% coordinates = Coordinate.where(id: props['ids']) %>
  <% coordinates.each do |coordinate| %>
  <div>
    <div>
    </div>
    <div>
      <% coordinate.products.each do |product| %>
      <div>
        <div>
          <%= product.name %>
        </div>
        <div>
          <%= product.price %>
        </div>
      </div>
      <% end %>
    </div>
  </div>
  <% end %>
EOF
Component.create(
  name: 'コーディネート一覧',
  code: 'coordinates',
  content: content
)
p "created #{Component.count} Component records."
