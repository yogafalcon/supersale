# Tag
%w[new sale catalog].each do |tag_code|
  Tag.create do |tag|
    tag.name = tag_code.capitalize
    tag.code = tag_code
    tag.image.attach(io: File.open(Rails.root.join("db/seeds/development/icons/#{tag_code}.png")), filename: "#{tag_code}.png")
  end
end
p "created #{Tag.count} Tag records."
