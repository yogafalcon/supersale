# Admin
Admin.roles.keys.each do |role|
  Admin.create do |admin|
    admin.email = "#{role}@example.com"
    admin.password = "#{role}1234"
    admin.role = role
    admin.avatar.attach(io: File.open(Rails.root.join('db/seeds/development/images/avatar/avatar.jpeg')), filename: 'avatar.jpeg')
  end
end

p "created #{Admin.count} Admin records."
