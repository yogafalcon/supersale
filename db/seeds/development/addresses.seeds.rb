# Address
after 'development:users', 'prefectures' do
  User.order('rand()').limit(10).each do |user|
    Random.rand(1..3).times do
      Address.create do |address|
        address.user = user
        address.first_name = Faker::Name.first_name
        address.last_name = Faker::Name.last_name
        address.first_kana = address.first_name.downcase
        address.last_kana = address.last_name.downcase
        address.postal_code = Faker::Address.postcode
        address.prefecture = Prefecture.order('rand()').first
        address.address_1 = Faker::Address.city
        address.address_2 = Faker::Address.street_address
        address.address_3 = Faker::Address.secondary_address
        address.tel_no = Faker::PhoneNumber.cell_phone
      end
    end
  end
  p "created #{Address.count} Address records."
end
