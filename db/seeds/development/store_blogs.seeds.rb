# StoreBlog
after 'development:stores' do
  Store.all.each do |store|
    StoreBlog.create do |store_blog|
      store_blog.store = store
      store_blog.name = Faker::Address.state
      store_blog.subject = "主題:#{Faker::Lorem.sentence}"
      store_blog.content = "内容:#{Faker::Lorem.paragraphs.join("\n")}"
      store_blog.priority = Random.rand(1..10)
      store_blog.image.attach(io: File.open(Rails.root.join("db/seeds/development/images/store_blog/store_blog_0#{Random.rand(1..4)}.jpg")), filename: 'store_blog.jpg')
    end
  end
end
