# Ranking
10.times do
  fake = Faker::Commerce.department
  Ranking.find_or_create_by(code: fake.gsub(/(, | & | )/, '_').underscore) do |ranking|
    ranking.name = "#{fake} Ranking"
  end
end
p "created #{Ranking.count} Ranking records."
