# User
(1..50).each do |i|
  User.create do |user|
    # user.skip_confirmation!
    user.email = "user_#{i}@example.com"
    user.password = 'user1234'
    user.avatar.attach(io: File.open(Rails.root.join('db/seeds/development/images/avatar/avatar.jpeg')), filename: 'avatar.jpeg')
    user.nickname = Faker::Internet.user_name
    user.birthday = Faker::Date.birthday(18, 40)
    user.sex = :female
  end
end

p "created #{User.count} User records."
