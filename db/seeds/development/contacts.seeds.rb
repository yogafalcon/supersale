# Contact
50.times do
  Contact.create do |contact|
    contact.name = Faker::Name.name
    contact.email = Faker::Internet.email
    contact.category = Contact.categories.keys.sample
    contact.content = Faker::Lorem.paragraphs.join("\n")
  end
end
p "created #{Contact.count} Contact records."
