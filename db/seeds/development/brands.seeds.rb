after 'development:companies' do
  # Brand
  2.times do
    fake = Faker::Pokemon.name
    Brand.find_or_create_by(code: fake.gsub(/(, | & | )/, '_').underscore) do |brand|
      brand.company = Company.order('rand()').first
      brand.name = fake
    end
  end
  p "created #{Brand.count} Brand records."
end
