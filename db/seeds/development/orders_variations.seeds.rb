after 'development:orders', 'development:variations' do
  # OrdersVariation
  Order.all.each do |order|
    Variation.order('rand()').limit(Random.rand(1..3)).each do |variation|
      OrdersVariation.create do |order_variation|
        order_variation.order = order
        order_variation.variation = variation
        order_variation.quantity = 1
      end
    end
  end
  p "created #{OrdersVariation.count} OrdersVariation records."
end
