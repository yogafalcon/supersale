# Size
%w[s m l].each do |size|
  Size.create(name: size.upcase, code: size)
end
p "created #{Size.count} Size records."
