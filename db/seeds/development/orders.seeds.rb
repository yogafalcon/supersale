after 'development:users' do
  # Order
  Order.categories.keys.each do |category|
    30.times do
      Order.create do |order|
        order.category = category
        order.status = Order.statuses.keys.sample
        order.payment_status = Order.payment_statuses.keys.sample
        order.user = User.order('rand()').first if Random.rand(0..5) == 0
        order.prefecture = Prefecture.order('rand()').first
      end
    end
  end
  p "created #{Order.count} Order records."
end
