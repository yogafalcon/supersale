after 'categories', 'development:products' do
  # CategoriesProduct
  Category.all.each do |category|
    Product.order('rand()').limit(Random.rand(20..40)).each do |product|
      CategoriesProduct.create do |categories_product|
        categories_product.product = product
        categories_product.category = category
      end
    end
  end
  p "created #{CategoriesProduct.count} CategoriesProduct records."
end
