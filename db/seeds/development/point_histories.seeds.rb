after 'development:users' do
  # PointHistory
  User.all.each do |user|
    point = Random.rand(1000..5000).truncate(-2)
    limited_time_point = [0, 1000, 2000].sample
    user.point_histories.build(category: :normal, content: '引継', point: point)
    user.point_histories.build(category: :limited_time, content: '引継', point: limited_time_point) if limited_time_point > 0
    20.times do
      category = PointHistory.categories.keys.sample
      temp_point = Random.rand(-1000..1000).truncate(-1)
      if category == 'normal'
        point += temp_point
        break if point < 0
        user.point_histories.build(category: category, content: "buy #{Faker::Commerce.product_name}", point: temp_point)
      elsif category == 'limited_time'
        limited_time_point += temp_point
        break if limited_time_point < 0
        user.point_histories.build(category: category, content: "buy #{Faker::Commerce.product_name}", point: temp_point)
      else
        limited_time_point += temp_point.abs * -1
        break if limited_time_point < 0
        user.point_histories.build(category: category, content: "expired", point: temp_point)
      end
    end
    user.save
  end
  p "created #{PointHistory.count} PointHistory records."
end
