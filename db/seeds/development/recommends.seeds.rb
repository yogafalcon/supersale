after 'development:products' do
  # Recommend(also_bought)
  Product.order('rand()').limit(30).each do |product|
    Product.where.not(id: product.id).order('rand()').limit(5).each do |recommend_product|
      Recommend.create(product: product, recommend_product: recommend_product, category: :also_bought)
    end
  end
  p "created #{Recommend.count} Recommend records."
end
