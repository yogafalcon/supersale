after 'development:products', 'development:tags' do
  # ProductsTag
  Product.all.each do |product|
    tag_limit = Random.rand(0..2)
    Tag.limit(tag_limit).each do |tag|
      ProductsTag.create do |products_tag|
        products_tag.product = product
        products_tag.tag = tag
      end
    end
  end
  p "created #{ProductsTag.count} ProductsTag records."
end
